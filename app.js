var express = require('express'),
    http = require('http'),
    app = express(),
    routes = require('./routes');

app.set('port', 3001);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + "/public"));
routes.initialize(app);


http.createServer(app).listen(app.get('port'), console.error);
