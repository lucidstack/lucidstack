module.exports.initialize = function initialize(app) {
  app.get('/', function(req, res) {
    res.render('index', {title: "lucidStack"});
  });
};
